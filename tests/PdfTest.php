<?php

declare(strict_types=1);

namespace Tests\Pdf;

use GuzzleHttp\Exception\ClientException;
use PHPUnit\Framework\TestCase;
use Wyzen\HtmlToPdf;

class PdfTest extends TestCase
{
    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $parsed = \parse_ini_file(__DIR__ . '/.env');

        $this->htmlToPdf = new HtmlToPdf($parsed['HTMLTOPDF_URL']);
        $this->htmlToPdf->setAuth($parsed['HTMLTOPDF_ACCOUNT'], $parsed['HTMLTOPDF_KEY']);
    }

    public function testClassExists()
    {
        $this->assertEquals('Wyzen\HtmlToPdf', HtmlToPdf::class);
    }

    public function testConnexionApi()
    {
        $response = $this->htmlToPdf->testConnection()->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetHelp()
    {
        $html = $this->htmlToPdf->getHelp();
        $this->assertNotEmpty($html);
    }

    public function testPdfFromHtmlBadParameter()
    {
        $this->expectException(ClientException::class);
        $options  = [];
        $response = $this->htmlToPdf->toPdf($options)->getResponse();
    }

    public function testPdfFromHtml()
    {
        $options = [
            "html" => "<h2>PDF from html code inline</h2>",
        ];

        $response = $this->htmlToPdf->toPdf($options)->getResponse();

        $this->assertEquals('application/pdf', $response->getHeader('Content-Type')[0]);

        $this->htmlToPdf->writeToFile(__DIR__ . '/file_from_html.pdf');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPdfFromHtmlWithHeaderFooter()
    {
        $options = [
            "html" => "<h2>PDF from html code inline</h2>",
            "options" => [
                "common" => [
                    "javascript-delay" => 1000,
                    "no-stop-slow-scripts" => true
                ],
                "pdf" => [
                    "orientation" => "Portrait",
                    "header-center" => "From Html to Pdf Generator",
                    "footer-center" => "from Wyzen",
                ],
            ],
        ];

        $response = $this->htmlToPdf->toPdf($options)->getResponse();

        $this->assertEquals('application/pdf', $response->getHeader('Content-Type')[0]);

        $this->htmlToPdf->writeToFile(__DIR__ . '/file_from_html_custom_params.pdf');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPdfFromUrlGoogle()
    {
        $options = [
            "url" => "https://www.google.com",
            "options" => [
                "common" => [
                    "javascript-delay" => 1000,
                    "no-stop-slow-scripts" => true
                ],
                "pdf" => [
                    "orientation" => "Portrait",
                    "header-center" => "From google.com",
                    "footer-center" => "from Wyzen",
                ],
            ],
        ];

        $response = $this->htmlToPdf->toPdf($options)->getResponse();

        $this->assertEquals('application/pdf', $response->getHeader('Content-Type')[0]);

        $this->htmlToPdf->writeToFile(__DIR__ . '/file_from_google.pdf');

        $this->assertEquals(200, $response->getStatusCode());
    }










    public function testImageFromHtmlBadParameter()
    {
        $this->expectException(ClientException::class);
        $options  = [];
        $response = $this->htmlToPdf->toImage($options)->getResponse();
    }

    public function testImageFromHtml()
    {
        $options = [
            "html" => "<h2>PDF from html code inline</h2>",
            "options" => [
                "image" => [
                    "format" => "png",
                    "width" => 800,
                    "height" => 600,
                ],
            ]
        ];


        $response = $this->htmlToPdf->toImage($options)->getResponse();

        $this->assertEquals('image/png', $response->getHeader('Content-Type')[0]);

        $this->htmlToPdf->writeToFile(__DIR__ . '/file_from_html.png');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testImageFromHtmlWith800x600()
    {
        $options = [
            "html" => "<h2>PDF from html code inline</h2>",
            "options" => [
                "common" => [
                    "javascript-delay" => 1000,
                    "no-stop-slow-scripts" => true
                ],
                "image" => [
                    "format" => "jpg",
                    "width" => 800,
                    "height" => 600,
                ],
            ],
        ];


        $response = $this->htmlToPdf->toImage($options)->getResponse();

        $this->assertEquals('image/jpg', $response->getHeader('Content-Type')[0]);

        $this->htmlToPdf->writeToFile(__DIR__ . '/file_from_html_custom_params.png');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testImageFromUrlGoogle()
    {
        $options = [
            "url" => "https://www.google.com",
            "options" => [
                "common" => [
                    "javascript-delay" => 1000,
                    "no-stop-slow-scripts" => true
                ],
                "image" => [
                    "format" => "png",
                    "width" => 800,
                    "height" => 600,
                ],
            ],
        ];


        $response = $this->htmlToPdf->toImage($options)->getResponse();

        $this->assertEquals('image/png', $response->getHeader('Content-Type')[0]);

        $this->htmlToPdf->writeToFile(__DIR__ . '/file_from_google.png');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testImageJpegFromHtmlWith800x600()
    {
        $options = [
            "html" => "<h2>PDF from html code inline</h2>",
            "options" => [
                "common" => [
                    "javascript-delay" => 1000,
                    "no-stop-slow-scripts" => true
                ],
                "image" => [
                    "format" => "jpeg",
                    "width" => 800,
                    "height" => 600,
                ],
            ],
        ];


        $response = $this->htmlToPdf->toImage($options)->getResponse();

        $this->assertEquals('image/jpeg', $response->getHeader('Content-Type')[0]);

        $this->htmlToPdf->writeToFile(__DIR__ . '/file_from_html_custom_params.jpg');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
