<?php

namespace Wyzen;

use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class HtmlToPdf
{
    private $url      = null;
    private $token    = 'emptyToken';
    private $options  = [];
    private $client   = null;
    private $response = null;

    /**
     * construct
     *
     * @param string $url
     * @param array|null $guzzleOptions
     */
    public function __construct(string $base_uri, ?array $guzzleOptions = [])
    {
        $this->url    = $base_uri;
        $this->client = new Client(['base_uri' => $base_uri] + $guzzleOptions);
    }

    /**
     * Set options
     *
     * @param array $options
     *
     * @return array
     */
    public function setOptions(array $options): array
    {
        unset($options['base_uri']);
        $this->options = \array_merge($this->options, $options);
        return $this->options;
    }

    /**
     * Retourne le token
     *
     * @param string $accountName
     * @param string $signatureKey
     * @param string $alg
     *
     * @return void
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Set le token
     *
     * @param string $token
     *
     * @return self
     */
    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    /**
     * Set l'auth de connexion
     *
     * @param string $accountName
     * @param string $signatureKey
     * @param string|null $alg
     *
     * @return self
     */
    public function setAuth(string $accountName, string $signatureKey, ?string $alg = 'HS256'): self
    {
        $jwt = JWT::encode([
            'account' => $accountName,
        ], $signatureKey, $alg);
        $this->setToken($jwt);
        return $this;
    }

    /**
     * Retourne les headers
     *
     * @return array
     */
    private function buildHeaders(): array
    {
        $options = $this->client->getConfig();
        $options['headers']['Content-Type']  = 'application/json';
        $options['headers']['authorization'] = 'Bearer ' . $this->getToken();
        return $options;
    }

    /**
     * Test la connexion à l'api
     *
     * @return self
     */
    public function testConnection(): self
    {
        $this->response = $this->client->get('/', $this->buildHeaders());
        return $this;
    }

    /**
     * Appel de la génération du pdf
     *
     * @param array $options
     *
     * @return self
     */
    public function toPdf(array $options = []): self
    {
        $headers = $this->buildHeaders();
        if ($options) {
            $headers = $headers + ['body' => \json_encode($options)];
        }
        $this->response = $this->client->post('/to-pdf', $headers);
        return $this;
    }

    /**
     * Appel de la génération de l'image
     *
     * @param array $options
     *
     * @return self
     */
    public function toImage(array $options = []): self
    {
        $headers = $this->buildHeaders();
        if ($options) {
            $headers = $headers + ['body' => \json_encode($options)];
        }
        $this->response = $this->client->post('/to-image', $headers);
        return $this;
    }

    /**
     * Retourne la response de l'api
     *
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * Retourne les données
     *
     * @return string
     */
    public function getContent(): string
    {
        if ($this->response) {
            return $this->response->getBody();
        }
        return 'No Content';
    }

    /**
     * Ecrit le contenu dans un fichier
     *
     * @param string $outputfile
     * @param boolean $rewrite default(true)
     *
     * @return void
     */
    public function writeToFile(string $outputfile, bool $rewrite = true)
    {
        if (\file_exists($outputfile)) {
            if ($rewrite) {
                \unlink($outputfile);
            } else {
                return false;
            }
        }

        \file_put_contents($outputfile, $this->getContent());
        return $outputfile;
    }

    /**
     * Retourne la documentation HTML
     *
     * @return string
     */
    public function getHelp(): string
    {
        $this->response = $this->client->get('/parameters', $this->buildHeaders());
        return $this->getContent();
    }
}
