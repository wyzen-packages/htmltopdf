# HTML to PDF API caller
Librairie pour l'utilisation du service HTMLtoPDF Wyzen

## Required
Nécessite le service htmlToPdf Wyzen

## Import
```bash
composer require wyzen-packages/htmltopdf
```

## Options

### from URL or HTML
- depuis une source HTML ou HTML64
```php
    $options = [
            "html" => "<h2>PDF from html code inline</h2>",
            //"html64" => "html encoded base64"
    ]
```
- depuis une source URL
```php
    $options = [
            "url" => "https://www.google.com",
    ]
```

### common
```php
    $options = [
            "options" => [
                "common" => [
                    "javascript-delay" => 1000,
                    "no-stop-slow-scripts" => true
                ],
            ],
        ];
```

### pdf
```php
    $options = [
            "options" => [
                "pdf" => [
                    "orientation" => "Portrait",
                    "header-center" => "My header",
                    "footer-center" => "My footer",
                ],
            ],
        ];
```

### image
```php
    $options = [
            "options" => [
                "image" => [
                    "format" => "png",
                    "width" => 800,
                    "height" => 600,
                ],
            ],
        ];
```

## usage
```php
use Wyzen\HtmlToPdf;

// Set URL
$htmlToPdf = new HtmlToPdf($_ENV['HTMLTOPDF_URL']);

// Set Auth
$htmlToPdf->setAuth($_ENV['HTMLTOPDF_ACCOUNT'], $_ENV['HTMLTOPDF_KEY']);

// Get HTML Help
$html = $htmlToPdf->getHelp();

// Get PDF response
$response = $htmlToPdf->toPdf($options)->getResponse();

// Get PDF content
$response = $htmlToPdf->toPdf($options)->getContent();

// Get Image response
$response = $htmlToPdf->toImage($options)->getResponse();

// Get Image content
$response = $htmlToPdf->toImage($options)->getContent();
```
